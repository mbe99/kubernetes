# LB3 - Kubernetes 

Für eine speditive Bewertung führen sie bitte alle nachfolgenden Befehle auf ihrer lokalen Installation aus und halte die Resultate in einer Text-Datei fest. 

Falls sie über den Umfang der Anleitung hinaus weitere Konfigurationen (etwa weitere Deployment) vorgenommen haben, halten sie diese ebenfalls in dieser Textdate fest. 

Benennen sie die Textdatei wie folgt: `lb2_Vorname_Nachname.txt`

> Senden sie die Datei per Mail an **marco.berger@tbz.ch**


## Befehle

**UUID**

`kubectl describe nodes |grep ID:`

- [ ] erfüllt
- [ ] teilweise erfüllt
- [ ] nicht erfüllt




**Cluster Node**

`kubectl get node -o wide`

- [ ] erfüllt
- [ ] teilweise erfüllt
- [ ] nicht erfüllt

**Pods**

`kubectl get pod --all-namespaces`

- [ ] erfüllt
- [ ] teilweise erfüllt
- [ ] nicht erfüllt

**Service**

`kubectl get service --all-namespaces`

- [ ] erfüllt
- [ ] teilweise erfüllt
- [ ] nicht erfüllt

**Deployment**

`kubectl get deployments.apps`

- [ ] erfüllt
- [ ] teilweise erfüllt
- [ ] nicht erfüllt

**Persistent Volume**

`kubectl get persistentvolume`

- [ ] erfüllt
- [ ] teilweise erfüllt
- [ ] nicht erfüllt

## Weitere Konfigurationen

```
möglichst genaue Beschreibung welche weiteren Konfigurationen sie vorgenommen haben
```

`code `` `

