[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md 
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/ 
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/


[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# Zertifikate für das Kubernetes-Dashboard 

Das Kubernetes-Dashboard benötigt ein Zertifikat. Für die Lab-Umgebung wird ein *self-signed* Zertifikat verwendet.

> Eigene Zertifikate sind nicht zwingend nötig. Ohne würden `auto-generate-certificates` genenriert werden.

 Die Zertifikat Files werden unter `$HOME/certs` gespeichert. Die Anleitung geht von folgenden Filenamen aus:

* `tls.crt`
* `tls.key` 

```
# mkdir $HOME/certs
# cd $HOME/certs
```

## self-signed Certificate

### SSL Keys erzeugen

Nachfolgend wird ein [Self-Signed Certifikate][10] erstellt, welches für das Dashboard verwendet werden kann.

> für die Testinstallation kann ein unsicheres Passwort verwendet werden

Zuerst wird ein encrypted Keypair File erzeugt und nach `keypair.key` geschrieben. 

```
# openssl genrsa -des3 -passout pass:kubepass -out keypair.key 2048
```

In einem zweiten Schrit wird der private key extrahiert undf nach `tls.key` geschrieben.

```
# openssl rsa -passin pass:kubepass -in keypair.key -out tls.key
```

Das `keypair.key` wird nun nicht mehr benötigt und kann gelöscht werden.


### generate tls.csr

Zum generieren des Zertifikat folgenden Befehl eingeben.

```
# openssl req -new -key tls.key -out tls.csr
```

Nachfolgende Werte für das *self-signed* Zertifikat nach eigenen Vorgaben setzen. 

|Key | Value |
|:-- |:-- |
|Country Name (2 letter code) |CH|
|State or Province Name |Zuerich|
|Locality Name  |Zuerich|
|Organization Name |someOrg|
|Organizational Unit Name |someUnit|
|Common Name |master.k8s|
|Email Address |some.one@k8s|
|A challenge password | kubepass|
|An optional company name ||


### generate tls.crt

```
# openssl x509 -req -days 365 -in tls.csr -signkey tls.key -out tls.crt
```

Nun sind die nötigen Zertifikate generiert

```
-rw-r--r-- 1 root root 1302 Apr 17 11:05 tls.crt
-rw-r--r-- 1 root root 1115 Apr 17 11:04 tls.csr
-rw------- 1 root root 1679 Apr 17 11:02 tls.key
```

[zurück](README.md) | [weiter](dashboard.md) | [Home](../../README.md)


