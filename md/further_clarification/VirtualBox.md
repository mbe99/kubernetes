[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/
[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# VirtualBox Network Configuration

## Die verschiedenen VirtualBox Netzwerke

Bei Virtual Box so wie bei allen anderen Virtualisierungssoftwares gibt es verschiedene Netzwerktypen. Jedes dieser Netzwerke kann für einen anderen zweck verwendet werden und hat somit seine eigenen Vorteile. 

Um einen genauen überblick zu erhalten kann man diese Seite verwenden:
https://www.virtualbox.org/manual/ch06.html

Um jedoch einen kleinen Überblick zu erhalten hier einen kleine Tabelle:
| Mode       | VM --> Host | VM <-- Host  | VM1 <--> VM2 | VM --> Net/LAN | VM <-- Net/LAN |
|------------|-------------|--------------|--------------|----------------|----------------|
| Host-Only  |      +      |       +      |       +      |        -       |        -       |
| Internal   |      -      |       -      |       +      |        -       |        -       |
| Bridged    |      +      |       +      |       +      |        +       |        +       |
| NAT        |      +      | Port Forward |       -      |        +       |  Port Forward  |
| NATservice |      +      | Port Forward |       +      |        +       |  Port Forward  |


## Was wird verwendet
Da wir für unser Vorhaben  Host <--> VM und VM1 <--> VM2 Netzwerkverkehr benötigen (Damit das Cluster kommunizieren kann). Dies aber nicht unbedingt ins Internet publiziert werden sollte, verwenden wir das Host-Only Netzwerk. 

Da aber gewisse Applikationen aus dem Internet heruntergeladen werden müsssen, benötigen wir auch eine Schnittstelle für das Internet. 

Aus diesem Grund müssen alle von uns aufgesetzten VM's **zwei Netzwerkadapter** besitzten. Der eine wird mit **Host-Only** aufgesetzt und der andere mit **NAT**.

## Konfiguration des Netzwerks
Damit wir unser Host-Only Netzwerk entsprechend verwenden können, müssen wir dieses noch anpassen.
Der einfachste Weg dies zu tun ist über das Virtual Box GUI.

Um in die Einstellungen der Netzwerke in VirtualBox zu kommen muss man im Hauptmenü auf Werkzeuge klicken.
![Werkzeuge](../../images/Werkzeuge.png)

Anschliessend kann man entweder ein neues Netzwerk erstellen oder ein bereits existierendes bearbeiten.
In diesem Fall werde ich ein bereits existierendes bearbeiten.
![Netzwerk Config](../../images/Netwerk_Config.png)
 
Auf diesem Bild ist auch zu sehen, was für Netzwerkeinstellungen benötigt werden. 
Unter dem Tab `DHCP-Server` muss man noch den Server deaktivieren und anschliessend ist dieses Netzwerk funktionsbereit.

[zurück](README.md) | [Home](../../README.md) | [weiter](CentOS.md) 