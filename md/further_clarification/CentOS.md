[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/
[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# CentOS Network Settings
Bei CentOS gibt es verschiedene Möglichkeiten um die lokalen Netzwerkeinstellungen zu ändern.

Zum einen kann man im File `/etc/sysconfig/network-scripts/ifcfg-[interface-name]` die Einstellungen anpassen.
Des weiteren ist es auch möglich mit einem kleinen Interface die Einstellungen vorzunehmen. 

## Konfiguration im File
Damit man weiss in welches file man schreiben muss, kann man mithilfe von `nmcli d` die angeschlossenen Netzwerkkarten ausgeben. 

Um einen Netzwerkadapter zu aktivieren, und diesem eine statische IP Adresse zuzuweisen, kann man in das ensprechende File folgendes reischreiben: 
```
DEVICE=enp0s8
ONBOOT=yes
IPADDR=192.168.11.100
PREFIX=24
```
Um nun die Einstellungen zu aktivieren, muss man die konfigurierten Interfaces "bouncen". Das bedeutet aus und einstellen, um die neue Konfiguration zu laden. 
Dies kann mit diesem Befehel gemacht werden: 
```
systemctl restart NetworkManager.service
```

## Konfiguration über das UI

CentOS bietet auch ein Interface, mit welchem die Einstellungen geändert werden können.

Das Interface kann man mit dem Befehl `nmtui` aufrufen.
Wenn das UI aufgerufen wurde, kann man ein Interface unter dem Reiter "Activate a connection" aktivieren. 


![Activate a connection](../../images/activate_connection.png)

Anschliessend kann man das Interface aussuchen und dieses mit Activate aktivieren


![Activate](../../images/activate.png)

Auch in diesem Interface ist es möglich die IP Adresse zu setzten so wie einzustellen, dass die Netzwerkkarte immer aktiv gestellt werden sollte.
Um dies zu machen muss man den "Edit a connection" auswählen.


![Edit a connection](../../images/edit_connection.png)

Dort kann man anschliessend das Interface aussuchen welches man bearbeiten will und dieses mit "Edit" bearbeiten. 


![Edit](../../images/edit.png)

Anschliessend kann man dort die wichtigen Einstellungen vornehmen. Um das Interface automatisch immer zu aktivieren muss man "Automaticaly connect" auswählen. 


![UI Netzwerk config](../../images/network_config.png)

Nachdem diese Änderungen vorgenommen wurden, kann man das UI wieder schliessen. In diesem Fall ist es auch nicht nötig den Service neu zu laden.

Mit diesem UI ist es auch sehr einfach möglich den Hostnamen des Systemes zu ändern.

[zurück](VirtualBox.md) | [Home](../../README.md) 