[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8/
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/
[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# Secrets

Secrets ermöglichen es, Passwörter, API-Token, SSL Keys, Zerifikate usw. einem POD zu übergeben. Standardmässig sind Secrets *base64 encoded*, also nicht verschlüsselt. Die Secrets werden in `etcd` abgelegt und können nur von PODs innerhalb desselben Namespaces referenziert werden.

Informationen wie Secrets verschlüsselt werden können finden sich [hier][70]

Secrets werden als *key-value* abgelegt

``` 
PASSWORT=topsecret
```




> ACHTUNG: PODs starten nicht, wenn definierte Secrets nicht verfügbar sind.

## Arten von Secrets

* Generic
* Docker Registry
* TLS


## Secrets bereitstellen

Secrets können über verschiedene Wege einem Pod zur Verfügung gestellt werden.

* Command Line Argument
* Environment Variables
* ConfigMaps

### Environment Variables

Es existieren zwei Typen von Environment Variablen

* User defined
* System definied

User definied Variables werden für jeden Container im `Pod Spec` definiert.

> Variablen werden zum Startzeitpunk des Container definiert
> und werden danach nicht mehr aktuallisiert.







