# Kubernetes Volumes

1. [NFS Server](nfsserver.md)
2. [persistent Volume](persistent.md)
3. [persistent Volume claim](claim.md)

[Home](../../README.md)